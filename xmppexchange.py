import config
import logging
import threading

import chatexchange.client
import chatexchange.events

from sleekxmpp import ClientXMPP
from bs4 import BeautifulSoup


class XMPPExchange(ClientXMPP):
    relay = False

    def __init__(self, jid, password, log):
        # Set logger
        self.log = log

        # Initialize SleekXMPP
        ClientXMPP.__init__(self, jid, password)

        # Add XMPP event handlers for the stuff we'll use
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.on_xmpp_message)
        self.add_event_handler("changed_status", self.wait_for_presences)

        # ?? TODO figure out what this is for
        self.received = set()
        self.presences_received = threading.Event()

    def session_start(self, event):
        # Start XMPP Session
        self.send_presence()
        self.get_roster()
        self.presences_received.wait(5)
        self.log.info(f"Logged in to XMPP as {self.boundjid.bare}")

        # Start StackExchange Chat Session
        client = chatexchange.client.Client(config.SE_HOST)
        client.login(config.SE_EMAIL, config.SE_PASS)
        self.stackme = client.get_me()
        self.log.info(f"Logged in to StackExchange as {self.stackme.name}")

        # Get StackExchange Room
        self.stackroom = client.get_room(config.SE_ROOM)
        self.stackroom.join()
        self.stackroom.watch(self.on_stack_message)

    def set_relay(self, state):
        # Set state of relay and announce it to the user
        self.relay = bool(state)
        self.log.info(f"Relay status: {state}")

    def wait_for_presences(self, pres):
        # ???? me a year ago what'd you try to accomplish here?
        self.received.add(pres['from'].bare)

        # If the presence is from the relevant user, set relay status to it
        if (pres['from'].bare == config.RELAY_TO):
            # Enable relay if we're available and disable otherwise
            self.set_relay(pres.get_type() == "available")

        # I'm uh... I'm trying to comment old code but uhh
        # I have no fucking idea what I tried to accomplish here
        # I think that I get the first part but what's up with the second part?
        if len(self.received) >= len(self.client_roster.keys()):
            self.presences_received.set()
        else:
            self.presences_received.clear()

    def on_stack_message(self, message, client):
        # If the incoming event isn't a message, then ignore it
        # Also, ignore our own messages
        if not isinstance(message, chatexchange.events.MessagePosted)\
                or message.user is self.stackme:
            return

        # Check if we got pinged (first 3 letters of nick is enough)
        # If we're are, pass on to XMPP no matter what
        # If we're not and relay mode is off, do NOT pass it on to XMPP
        # If we're not and relay mode is on, do pass it on to XMPP
        if f"@{self.stackme.name}"[:4].lower() not in message.content.lower()\
                and not self.relay:
            return

        # Parse the contents of the message with bs4 in case it's an embed
        # And send the result to the XMPP user
        soup = BeautifulSoup(message.content, "html.parser")
        message_content = f"{message.user.name}: {soup.get_text()}"
        self.log.info(f"Stack {message_content}")
        self.send_message(mto=config.RELAY_TO,
                          mbody=message_content, mtype='chat')

    def on_xmpp_message(self, msg):
        # Ignore non-chat messages and messages that aren't from the XMPP user
        if msg['type'] not in ('chat', 'normal')\
                and (msg['from'].bare == config.RELAY_TO):
            return

        # Log the incoming message
        self.log.info(f"XMPP {msg['from'].bare}: {msg['body']}")

        # Check for the .relay command
        if msg['body'].startswith(".relay"):
            # Split message by space to
            # 1) check if a value is given to set
            # 2) get the value to set easily
            msg_split = msg['body'].split(" ")

            # If there's value given to set
            if len(msg_split) != 1:
                # set it to the value
                self.set_relay(msg_split[1])

            # Notify user of the relay state
            self.send_message(mto=config.RELAY_TO,
                              mbody=f"Relay state: {self.relay}",
                              mtype='chat')
        else:
            # Send the message to the stack room
            self.stackroom.send_message(msg['body'])


if __name__ == '__main__':
    # Set up logging
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')
    log = logging.getLogger(__name__)

    # Login to XMPP
    xmpp = XMPPExchange(config.RELAY_FROM, config.RELAY_FROM_PASS, log)
    xmpp.connect()
    xmpp.process(block=True)
