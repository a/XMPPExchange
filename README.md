# XMPPExchange
Connect to StackExchange Chat with XMPP/Jabber.

Needs python3.6+

![](https://old-s.ave.zone/dcf.PNG) ![](https://old-s.ave.zone/4ce.png)

## Setup Instructions
- Install requirements through pip: `python3.6 -m pip install -Ur requirements.txt`
- Install `build-essentials` or `base-devel` or whatever your distro calls it, and also `python3-dev`.
- Clone https://github.com/Manishearth/ChatExchange somewhere.
- cd into it, edit the `Makefile` and replace all mentions of `python` with `python3`, and also replace `pip` with `python3.6 -m pip`. Run `make`. It's normal for `run-example` to fail.
- Go back to `XMPPExchange` folder, rename `config.py.example` to `config.py`, edit it with your credentials and such.
- Run it. `python3.6 xmppexchange.py`

## Features
- Bridging messages between `chat.stackexchange.com` / `chat.stackoverflow.com` and XMPP/Jabber
- Checking user's online status and not sending when offline (if not pinged)

## Limitations
- One room per XMPPExchange instance
- Online status checking is a little buggy with multiple XMPP clients running, though running `.relay True` enables relay in such tricky situations
